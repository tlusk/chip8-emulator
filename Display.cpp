/*
 * Cpu.cpp
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.9  2007/02/05 19:58:12  timothy
 *      Implemented a bitset
 *      Fixed scrolling right
 *      Fixed overflows when drawing which caused crashes
 *
 *      Revision 1.8  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.7  2007/01/24 03:41:11  timothy
 *      Partially implemented schip8
 *
 *      Revision 1.6  2007/01/23 08:04:43  timothy
 *      Implemented OpenGL drawing method
 *
 *      Revision 1.5  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.4  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 *      Revision 1.3  2007/01/21 18:02:57  timothy
 *      Added More Games
 *
 *      Revision 1.2  2007/01/18 03:25:03  timothy
 *      fixed the bug that broke drawing, added key presses
 *
 *      Revision 1.1  2007/01/18 01:53:40  timothy
 *      Initial Commit
 *
 */
 
#include <iostream>

#include "Display.h"
 
Display::Display( bool openglRender ) :
	opengl( openglRender ) {
	pixelSize = 8;
    	
	const int WINDOW_WIDTH = 128*4;
	const int WINDOW_HEIGHT = 64*4;
	const char* WINDOW_TITLE = "Chip-8 Emulator";
	
	SDL_Init( SDL_INIT_VIDEO );
	#ifndef PSP
	if( opengl ) {
		screen = SDL_SetVideoMode( WINDOW_WIDTH, WINDOW_HEIGHT, 16, SDL_OPENGL | SDL_HWACCEL );
	} else {
		screen = SDL_SetVideoMode( WINDOW_WIDTH, WINDOW_HEIGHT, 0, SDL_HWSURFACE | SDL_DOUBLEBUF );
	}
	#else
	if( opengl ) {
		screen = SDL_SetVideoMode( WINDOW_WIDTH, WINDOW_HEIGHT, 16, SDL_OPENGL | SDL_HWACCEL );
	} else {
		screen = SDL_SetVideoMode( WINDOW_WIDTH, WINDOW_HEIGHT, 16, SDL_SWSURFACE );
	} 
	#endif
	SDL_WM_SetCaption( WINDOW_TITLE, 0 );
	
	if( opengl ) {
		glClearColor( 0, 0, 0, 0 );
		
		glMatrixMode( GL_PROJECTION ); 
		glLoadIdentity(); 
		glOrtho( 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, -1, 1 );
		glMatrixMode( GL_MODELVIEW ); 
		glLoadIdentity();
	}
	
	clearScreen();
	
}

void Display::draw() {
	if( opengl ) {
		SDL_GL_SwapBuffers();
	} else {
		if( SDL_Flip( screen ) == -1 ) {
			exit(0);
    	}
	}
}

void Display::drawPixel( int x, int y, bool enabled ) {
	
	if( opengl ) {
		glTranslatef( x*pixelSize, y*pixelSize, 0 );
		
		glBegin( GL_QUADS );
			glColor4f( 1.0 * enabled, 1.0 * enabled, 1.0 * enabled, 1.0 * enabled );
			glVertex3f( 0, 0, 0 ); 
			glVertex3f( pixelSize, 0, 0 ); 
			glVertex3f( pixelSize, pixelSize, 0 ); 
			glVertex3f( 0, pixelSize, 0 );
		glEnd();
		
		glLoadIdentity();
	} else {
		boxRGBA( screen, x*pixelSize, y*pixelSize,((x+1)*pixelSize)-1, ((y+1)*pixelSize)-1, 255*enabled, 255*enabled, 255*enabled, 255);
	}
}
	
void Display::drawScreen( bitset<128*64> & disp ) {
	clearScreen();
	
	for( unsigned int y=0; y<(64/(pixelSize/4)); y++ ) {
		for( unsigned int x=0; x<(128/(pixelSize/4)); x++ ) {
			if( disp[x + ( y * (128/(pixelSize/4)) )] != 1 ) {
				if( opengl ) {
					glTranslatef( x*pixelSize, y*pixelSize, 0 );
					
					glBegin( GL_QUADS );
						glColor4f( 1.0, 1.0, 1.0, 1.0 );
						glVertex3f( 0, 0, 0 );
						glVertex3f( pixelSize, 0, 0 ); 
						glVertex3f( pixelSize, pixelSize, 0 ); 
						glVertex3f( 0, pixelSize, 0 );
					glEnd();
					
					glLoadIdentity();
				} else {
					boxRGBA( screen, x*pixelSize, y*pixelSize,((x+1)*pixelSize)-1, ((y+1)*pixelSize)-1, 255, 255, 255, 255);
				}
			}
		}
	}
	
	draw();
}

void Display::clearScreen() {
	if( opengl ) {
		glClear( GL_COLOR_BUFFER_BIT );
	} else {
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
	}
}

void Display::setExtendedMode( bool enabled ) {
	if( enabled ) {
		pixelSize = 4;
	} else {
		pixelSize = 8;
	}
}
