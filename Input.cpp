/*
 * Input.cpp
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.4  2007/02/06 00:40:29  timothy
 *      Added PSP Joystick Support
 *
 *      Revision 1.3  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.2  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.1  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 */

#include <iostream>

#include "Input.h"

Input::Input() {
	#ifdef PSP
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_JOYSTICK);
 	joystick = SDL_JoystickOpen(0);
  	SDL_JoystickEventState(SDL_ENABLE);
  	#endif 
}

unsigned char Input::getKeyPress() {
	SDL_Event event;
	bool pressed = false;
	unsigned char keyval = 0;
 
	while( !pressed ) {
		SDL_Delay( 10 );
		while ( SDL_PollEvent(&event) ) {
			if ( event.type == SDL_QUIT ) { 
				exit(0);
			}
			
			#ifndef PSP
			if( event.type == SDL_KEYDOWN ) {
				pressed = true;
         		
         		SDLKey key;
          		key = event.key.keysym.sym;
          		switch (key){
                 	case 256:
                      	keyval = 12;
                    break;      
                 
                 	case SDLK_KP1:
                      	keyval = 8;
                    break;
                 	
                 	case SDLK_KP2:
                      	keyval = 9;
                    break;
                 	
                 	case SDLK_KP3:
                      	keyval = 10;
                    break;
                 	
                 	case SDLK_KP4:
                      	keyval = 4;
                    break;
                    
                 	case SDLK_KP5:
                      	keyval = 5;
                    break;
                    
                 	case SDLK_KP6:
                      	keyval = 6;
                    break;
                    
                 	case SDLK_KP7:
                      	keyval = 0;
                    break;
                    
                 	case SDLK_KP8:
                      	keyval = 1;
                    break;
                    
                 	case SDLK_KP9:
                      	keyval = 2;
                    break;
                    
                 	case SDLK_KP_DIVIDE:
                      	keyval = 3;
                    break;
                    
                 	case SDLK_KP_MULTIPLY:
                      	keyval = 7;
                    break;
                    
                 	case SDLK_KP_MINUS:
                      	keyval = 11;
                    break;
                    
                 	case SDLK_KP_PERIOD:
                      	keyval = 13;
                    break;
                    
                 	case SDLK_KP_ENTER:
                      	keyval = 14;
                    break;
                    
                 	case SDLK_KP_PLUS:
                      	keyval = 15;
                    break;
                    
                 	default:
                      	pressed = false;
                    break;
				}
      		}
      		#else
      		if( event.type == SDL_JOYBUTTONDOWN ) {
      			switch (event.type) {
                	case SDL_JOYBUTTONDOWN:
                		pressed = true;
                    	printf("Joystick %d button %d down\n", event.jbutton.which, event.jbutton.button);
                    	keyval = event.jbutton.button;
                    break;
      			}
      		}
      		#endif
      		
     	}
     }
     
     return keyval;
}


bool Input::isKeyPressed( unsigned char key ) {
	#ifndef PSP
	SDLKey keyCode;
	Uint8 *keystates = SDL_GetKeyState( NULL );
	
	switch (key) {
      	case 12:
      		keyCode = (SDLKey) 256;
        break;      
     
     	case 8:
     		keyCode = SDLK_KP1;
        break;
     	
     	case 9:
     		keyCode = SDLK_KP2;
        break;
     	
     	case 10:
     		keyCode = SDLK_KP3;
        break;
     	
     	case 4:
     		keyCode = SDLK_KP4;
        break;
        
     	case 5:
     		keyCode = SDLK_KP5;
        break;
        
     	case 6:
     		keyCode = SDLK_KP6;
        break;
        
     	case 0:
     		keyCode = SDLK_KP7;
        break;
        
     	case 1:
     		keyCode = SDLK_KP8;
        break;
        
     	case 2:
     		keyCode = SDLK_KP9;
        break;
        
     	case 3:
     		keyCode = SDLK_KP_DIVIDE;
        break;
        
     	case 7:
     		keyCode = SDLK_KP_MULTIPLY;
        break;
        
     	case 11:
     		keyCode = SDLK_KP_MINUS;
        break;
        
     	case 13:
     		keyCode = SDLK_KP_PERIOD;
        break;
        
     	case 14:
     		keyCode = SDLK_KP_ENTER;
        break;
        
     	case 15:
     		keyCode = SDLK_KP_PLUS;
        break;
        
     	default:
          	//printf( "UNKNOWN KEY" );
          	return false;
        break;
	}

	if( keystates[ keyCode ] ) {
		return true;
	}
	
	return false;
	#else
	return SDL_JoystickGetButton(joystick, key);
	#endif
}
