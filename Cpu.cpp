/*
 * Cpu.cpp
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.13  2007/02/05 19:58:12  timothy
 *      Implemented a bitset
 *      Fixed scrolling right
 *      Fixed overflows when drawing which caused crashes
 *
 *      Revision 1.12  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.11  2007/01/24 03:41:11  timothy
 *      Partially implemented schip8
 *
 *      Revision 1.10  2007/01/23 08:04:43  timothy
 *      Implemented OpenGL drawing method
 *
 *      Revision 1.9  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.8  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 *      Revision 1.7  2007/01/21 18:02:57  timothy
 *      Added More Games
 *
 *      Revision 1.6  2007/01/21 08:07:36  timothy
 *      Fixed a warning with keyval
 *
 *      Revision 1.5  2007/01/21 08:06:39  timothy
 *      Added PSP Support (doesnt work on psp, just compiles)
 *      Fixed input handling (couldnt tell when keys were held)
 *      Fixed a bug when drawing (collision not reset before drawing)
 *      Optimized drawing
 *      Limited Speed (0% CPU!)
 *
 *      Revision 1.4  2007/01/19 23:43:28  timothy
 *      Added sound
 *      Added ability to choose the rom to run
 *
 *      Revision 1.3  2007/01/18 07:04:38  timothy
 *      Added timer countdown
 *
 *      Revision 1.2  2007/01/18 03:25:03  timothy
 *      fixed the bug that broke drawing, added key presses
 *
 *      Revision 1.1  2007/01/18 01:53:40  timothy
 *      Initial Commit
 *
 */

#include "Cpu.h"
#include <bitset>

Cpu::Cpu( unsigned char* mem, bool opengl ) :
    extendedMode( false ), memory( mem ), PC( 0x200 ), I( 0 ), stackPos( 0 ), display( opengl )  {
    	
    	for( int i = 0; i < 16; i++ ) {
    		V[i] = 0;
    	}
    	
    	unsigned char chip_sprites[80] = { 0xF0, 0x90, 0x90, 0x90, 0xF0,// 0
		0x20, 0x60, 0x20, 0x20, 0x70,// 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0,// 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0,// 3
		0x90, 0x90, 0xF0, 0x10, 0x10,// 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0,// 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0,// 6
		0xF0, 0x10, 0x20, 0x40, 0x40,// 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0,// 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0,// 9
		0xF0, 0x90, 0xF0, 0x90, 0x90,// A
		0xE0, 0x90, 0xE0, 0x90, 0xE0,// B
		0xF0, 0x80, 0x80, 0x80, 0xF0,// C
		0xE0, 0x90, 0x90, 0x90, 0xE0,// D
		0xF0, 0x80, 0xF0, 0x80, 0xF0,// E
		0xF0, 0x80, 0xF0, 0x80, 0x80 // F
		};
		
		unsigned char schip_sprites[10*10]= { 0x3C, 0x7E, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0x7E, 0x3C, /* 0 */
	 	0x18, 0x38, 0x58, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3C, /* 1 */
	 	0x3E, 0x7F, 0xC3, 0x06, 0x0C, 0x18, 0x30, 0x60, 0xFF, 0xFF, /* 2 */
	 	0x3C, 0x7E, 0xC3, 0x03, 0x0E, 0x0E, 0x03, 0xC3, 0x7E, 0x3C, /* 3 */
	 	0x06, 0x0E, 0x1E, 0x36, 0x66, 0xC6, 0xFF, 0xFF, 0x06, 0x06, /* 4 */
	 	0xFF, 0xFF, 0xC0, 0xC0, 0xFC, 0xFE, 0x03, 0xC3, 0x7E, 0x3C, /* 5 */
	 	0x3E, 0x7C, 0xC0, 0xC0, 0xFC, 0xFE, 0xC3, 0xC3, 0x7E, 0x3C, /* 6 */
	 	0xFF, 0xFF, 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x60, 0x60, /* 7 */
	 	0x3C, 0x7E, 0xC3, 0xC3, 0x7E, 0x7E, 0xC3, 0xC3, 0x7E, 0x3C, /* 8 */
	 	0x3C, 0x7E, 0xC3, 0xC3, 0x7F, 0x3F, 0x03, 0x03, 0x3E, 0x7C, /* 9 */
     	}; /* 8x10 pixel font patterns (only 10) */
		
		
		for( int i = 0; i < 80; i++ ) {
			//memory[0xF00 + i] = font[i];
			memory[i] = chip_sprites[i];
		}
		
		for( int i = 80; i < 180; i++ ) {
			memory[i] = schip_sprites[i-80];
		}
		
		for( int i = 0; i < (128*64); i++ ) {
			screen[i] = 0;
		}
		
		
}

void Cpu::exec() {
	
	unsigned int opcode;
	opcode = ((memory[PC]<<8) + memory[PC+1]);
	if ( dTimer > 0 ) {
		dTimer--;
	}
	if ( sTimer > 0 ) {
		sTimer--;
		if( sTimer == 0 ) {
			audio.beep();
			//cout << "*BEEP!*" << endl;
		}
	} 
	//printf( "%#u : PC \n", PC );
	//printf( "%#x : memory[PC] \n", memory[PC] );
	//printf( "%#x : memory[PC]<<8 \n", memory[PC]<<8 );
	//printf( "%#x : memory[PC+1] \n", memory[PC+1] );
	//printf( "%#x : opcode \n", opcode );
	//printf( "%#x : opcode&0xF000 \n\n", opcode&0xF000 );
	
	switch (opcode&0xF000){
		
		case 0x0000:
			
			switch( opcode&0x00F0 ) {
				
				case 0xC0:
					//scroll the screen down N lines  	SuperChip only
					screen<<=( 128 * (opcode&0x000F) );
					
					display.drawScreen( screen );
					PC+=2;
				break;
				
				case 0xE0:
				
					switch( opcode&0x000F ) {
				
						case 0x0:
							for( int i = 0; i < (64*128); i++ ) {
								screen[i] = 0;
							}
							
							display.clearScreen();
							
							PC += 2;
						break;
						
						case 0xE:
							stackPos--;
							/*printf("%04x: Sub Returned From\n", PC);
							printf("%04x: Sub Returned To\n", subStack[stackPos]);*/
							PC = subStack[stackPos];
							subStack[stackPos] = 0;
						break;
						
						default:
							printf("%04x: opcode not implemented.\n", opcode);
							exit(0);
						break;
						
					}
				
				break;
				
				case 0xF0:
				
					switch( opcode&0x000F ) {
				
						case 0xB:
							//scroll screen 4 pixels right  	SuperChip only
							for( unsigned int yline=0; yline<64; yline++ ) {
								for( int xpix=(64+(64*(unsigned int)extendedMode))-1; xpix>=0; xpix-- ) {
									if( xpix >= 4 ) {
										screen[xpix + ((yline)*(64+(64*extendedMode)))] = screen[(xpix - 4) + ((yline)*(64+(64*extendedMode)))];
									} else {
										screen[xpix +((yline)*(64+(64*extendedMode)))] = 0;
									}
								}
							}
							
							display.drawScreen( screen );
							PC+=2;
						break;
						
						case 0xC:
							//scroll screen 4 pixels left  	SuperChip only
							
							for( unsigned int yline=0; yline<64; yline++ ) {
								//unsigned char data = memory[ I + yline ]; //this retreives the byte for a give line of pixels
								for( unsigned int xpix=0; xpix<(64+(64*(unsigned int)extendedMode)); xpix++ ) {
									if( xpix <124 ) {
										screen[xpix +((yline)*(64+(64*extendedMode)))] = screen[xpix + 4 +((yline)*(64+(64*extendedMode)))];
									} else {
										screen[xpix +((yline)*(64+(64*extendedMode)))] = 0;
									}
								}
							}
							
							display.drawScreen( screen );
							PC+=2;
						break;

						case 0xD:
							cout << "Exiting..." << endl;
							exit( 0 ); 
						break;
						
						case 0xE:
							//disable extended screen mode  	SuperChip only
							display.setExtendedMode( false );
							extendedMode = false;
							PC+=2;
						break;
						
						case 0xF:
							//enable extended screen mode (128 x 64)  	SuperChip only
							display.setExtendedMode( true );
							extendedMode = true;
							PC+=2; 
						break;
						
						default:
							printf("%04x: opcode not implemented.\n", opcode);
							exit(0);
						break;
						
					}
					
				break;
				
				default:
					printf("%04x: opcode not implemented.\n", opcode);
					exit(0);
				break;
				
			}
		
		break;
		
		case 0x1000:
			/*printf("%04x: Jumped From\n", PC);
			printf("%04x: Jumped To\n", (opcode&0xFFF));*/
			PC = (opcode&0x0FFF);
		break;
		
		case 0x2000:
			/*cout << "Stack Position: " << stackPos+1 << endl;
			printf("%04x: Sub Jumped From\n", PC);
			printf("%04x: Sub Jumped To\n", (opcode&0xFFF));*/
			
			subStack[stackPos]= PC+2;
			stackPos++;
			PC = (opcode&0x0FFF);
		break;
		
		case 0x3000:
			if( V[((opcode&0x0F00)>>8)] == (opcode&0x00FF) ) {
				PC+=4;
			} else {
				PC+=2;
			}
		break;
		
		case 0x4000:
			if( V[((opcode&0x0F00)>>8)] != (opcode&0x00FF) ) {
				PC+=4;
			} else {
				PC+=2;
			}
		break;
		
		case 0x5000:
			if( V[((opcode&0x0F00)>>8)] == V[((opcode&0x00F0)>>4)] ) {
				PC+=4;
			} else {
				PC+=2;
			}
		break;
	
		case 0x6000:
			V[((opcode&0x0F00)>>8)] = (opcode&0x00FF);
			PC+=2;
		break;
		
		case 0x7000:
			V[((opcode&0x0F00)>>8)] += (opcode&0x00FF);
			
			PC+=2;
		break;
	
		case 0x8000:
		
			switch( opcode&0x000F ) {
				
				case 0x0:
					V[((opcode&0x0F00)>>8)] = V[((opcode&0x00F0)>>4)];
					PC+=2;
				break;
				
				case 0x1:
					V[((opcode&0x0F00)>>8)] = V[((opcode&0x0F00)>>8)] | V[((opcode&0x00F0)>>4)];

					PC+=2;
				break;
				
				case 0x2:
					/*printf("%x: (opcode&0x00FF)\n", (opcode&0x00FF));
					printf("%x: V[((opcode&0x0F00)>>8)]\n", V[((opcode&0x0F00)>>8)]);
					printf("%x: V[((opcode&0x00F0)>>4)]\n", V[((opcode&0x00F0)>>4)]);*/
					V[((opcode&0x0F00)>>8)] = V[((opcode&0x0F00)>>8)] & V[((opcode&0x00F0)>>4)];
					/*printf("%x: V[((opcode&0x0F00)>>8)]\n", V[((opcode&0x0F00)>>8)]);
					printf("%x: V[((opcode&0x00F0)>>4)]\n", V[((opcode&0x00F0)>>4)]);*/
					PC+=2;
				break;
				
				case 0x3:
					/*printf("%x: (opcode&0x00FF)\n", (opcode&0x00FF));
					printf("%x: V[((opcode&0x0F00)>>8)]\n", V[((opcode&0x0F00)>>8)]);
					printf("%x: V[((opcode&0x00F0)>>4)]\n", V[((opcode&0x00F0)>>4)]);*/
					V[((opcode&0x0F00)>>8)] = V[((opcode&0x0F00)>>8)] ^ V[((opcode&0x00F0)>>4)];
					/*printf("%x: V[((opcode&0x0F00)>>8)]\n", V[((opcode&0x0F00)>>8)]);
					printf("%x: V[((opcode&0x00F0)>>4)]\n", V[((opcode&0x00F0)>>4)]);*/
					PC+=2;
				break;
				
				case 0x4:
					if( ( V[((opcode&0x0F00)>>8)] > ( 0xFF - V[((opcode&0x00F0)>>4)] ) ) ) {
					//printf( "%x : memory[PC+1]&0x0001 \n", V[((opcode&0x0F00)>>8)] + V[((opcode&0x00F0)>>4)]);
						
						V[0xF] = 1;
					} else {
					//printf( "%x : memory[PC+1]&0x0001 \n", V[((opcode&0x0F00)>>8)] + V[((opcode&0x00F0)>>4)] );
						V[0xF] = 0;
					}
					V[((opcode&0x0F00)>>8)] += V[((opcode&0x00F0)>>4)];
					PC+=2;
				break;
				
				case 0x5:
					if( V[((opcode&0x0F00)>>8)] < V[((opcode&0x00F0)>>4)] ) {
						V[0xF] = 0;
					} else {
						V[0xF] = 1;
					}
					V[((opcode&0x0F00)>>8)] -= V[((opcode&0x00F0)>>4)];
					PC+=2;
				break;

				case 0x6:
					/*printf( "--------- 0x8X06 --------\n" );
					printf( "%u : memory[PC+1] \n", memory[PC+1] );
					printf( "%u : memory[PC+1]&0x0001 \n", memory[PC+1]&0x0001 );
					printf( "%u : memory[PC+1]>>1 \n", memory[PC+1]>>1 );*/
					V[0xF] = (V[(opcode&0x0F00)>>8]&0x1);
					V[((opcode&0x0F00)>>8)]>>=1;
					PC+=2;
				break;
				
				case 0x7:
					if( V[((opcode&0x0F00)>>8)] > V[((opcode&0x00F0)>>4)] ) {
						V[0xF] = 0;
					} else {
						V[0xF] = 1;
					}
					V[((opcode&0x0F00)>>8)] = V[((opcode&0x00F0)>>4)] - V[((opcode&0x0F00)>>8)];
					PC+=2;
				break;
				
				case 0xE:
					V[0xF] = ((V[((opcode&0x0F00)>>8)]&0x80)>>7);
					V[((opcode&0x0F00)>>8)]<<=1;
					PC+=2;
				break;
				
			}
			
		break;
	
		case 0x9000:
			if( V[((opcode&0x0F00)>>8)] != V[((opcode&0x00F0)>>4)] ) {
				PC+=4;
			} else {
				PC+=2;
			}
		break;
		
		case 0xA000:
			I = (opcode&0xFFF);
			PC+=2;
		break;
		
		case 0xB000:
			PC = V[0] + (opcode&0xFFF);
		break;
		
		case 0xC000:
			V[((opcode&0x0F00)>>8)] = rand() & (opcode&0xFF);
			PC+=2;
		break;
		
		case 0xD000:
		
			V[0xF]=0;
			bool pixelDraw;
			pixelDraw = false;
		
			if( (opcode&0x000F) == 0 ) {
				//printf("%04x: opcode not implemented.\n", opcode);
				unsigned char X = V[((opcode&0x0F00)>>8)];
				unsigned char Y = V[((opcode&0x00F0)>>4)];

				//printf("%04x: Y.\n", Y);
				
				for( unsigned int yline=0; yline<16; yline++ ) {
					unsigned int data = memory[ I + ( yline * 2 ) ]; //this retreives the byte for a give line of pixels
					data<<=8;
					data+=memory[ I + 1 + (yline*2)];
					for( unsigned int xpix=0; xpix<16; xpix++ ) {
						if( xpix + X +((yline + Y)*(64+(64*extendedMode))) < (128*64) ) {
							if ( ( data&(0x8000>>xpix ) ) !=0 ) {
								if ( screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]== 1) {
									V[0xF]=1; //there has been a collision
									//printf("%u: xpix.\n", xpix);
									//printf("%u: X.\n", X);
									//printf("%u: yline.\n", yline);
									//printf("%u: Y.\n", Y);
									//cout << xpix + X +((yline + Y)*(64+(64*extendedMode))) << endl;
									screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]=0; //note: coordinate registers from opcode
									//display.drawPixel( xpix + X, yline + Y, false );
								} else {
									//display.drawPixel( xpix + X, yline + Y, true );
									screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]=1; //note: coordinate registers from opcode
									pixelDraw = true;
								}
							}
						}
					}
				}
				
				
				PC+=2;
				
				if( pixelDraw ) {
					display.drawScreen( screen );
				}
			} else {
				//printf( "%#u : PC2 \n", PC );
				//printf("%04x: opcode not implemented.\n", opcode);
				unsigned char X = V[((opcode&0x0F00)>>8)];
				unsigned char Y = V[((opcode&0x00F0)>>4)];
				
				for( unsigned int yline=0; yline<(opcode&0x000F); yline++ ) {
					unsigned char data = memory[ I + yline ]; //this retreives the byte for a give line of pixels
					for( unsigned int xpix=0; xpix<8; xpix++ ) {
						if( xpix + X +((yline + Y)*(64+(64*extendedMode))) < (128*64) ) {
							if ( ( data&(0x80>>xpix ) ) !=0 ) {
								if ( screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]== 1) {
									V[0xF]=1; //there has been a collision
									screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]=0; //note: coordinate registers from opcode
									//display.drawPixel( xpix + X, yline + Y, false );
								} else {
									//display.drawPixel( xpix + X, yline + Y, true );
									screen[xpix + X +((yline + Y)*(64+(64*extendedMode)))]=1; //note: coordinate registers from opcode
									pixelDraw = true;
								}
							}
						}
					}
				}
				
				//printf( "%#u : PC3 \n", PC );
				PC+=2;
				//printf( "%#u : PC4 \n", PC );
				
				if( pixelDraw ) {
					display.drawScreen( screen );
				}
				//printf("%u: PC\n", PC);
			}
						
		break;
		
		case 0xE000:
		
			switch( opcode&0x00FF ) {
				
				case 0x9e:
					if( input.isKeyPressed( V[((opcode&0x0F00)>>8)] ) ) {
						/*cout << "Keys Matched" << endl;
						printf("%x: Key Pressed.\n", keyval);
						printf("%x: Key Checked.\n\n", V[((opcode&0x0F00)>>8)]);*/
						PC+=4;
					} else {
						/*cout << "Keys Not Matched" << endl;
						printf("%x: Key Pressed.\n", keyval);
						printf("%x: Key Checked.\n\n", V[((opcode&0x0F00)>>8)]);*/
						PC+=2;
					}
						
				break;
				
				case 0xa1:
					if( !input.isKeyPressed( V[((opcode&0x0F00)>>8)] ) ) {
						/*cout << "Keys Not Matched" << endl;
						printf("%x: Key Pressed.\n", keyval);
						printf("%x: Key Checked.\n\n", V[((opcode&0x0F00)>>8)]);*/
						PC+=4;
					} else {
						/*cout << "Keys Matched" << endl;
						printf("%x: Key Pressed.\n", keyval);
						printf("%x: Key Checked.\n\n", V[((opcode&0x0F00)>>8)]);*/
						PC+=2;
					}
				break;
				
				default:
					printf("%04x: opcode not implemented.\n", opcode);
					exit(0);
				break;
				
			}
			
		break;
		
		case 0xF000:
			
			switch( opcode&0x00FF ) {
			
				case 0x07:
					V[((opcode&0x0F00)>>8)] = dTimer;
					PC+=2;
				break;
				
				case 0x0A:
					V[((opcode&0x0F00)>>8)]= input.getKeyPress();
					PC+=2;
				break;
				
				case 0x15:
					dTimer = V[((opcode&0x0F00)>>8)];
					PC+=2;
				break;
				
				case 0x18:
					sTimer = V[((opcode&0x0F00)>>8)];
					PC+=2;
				break;
			
				case 0x1E:
					I += V[((opcode&0x0F00)>>8)];
					PC+=2;
				break;
				
				case 0x29:
					/*printf( "%#x : V[(opcode&0x0F00)>>8] \n", V[(opcode&0x0F00)>>8] );
					printf( "%#x : V[(opcode&0x0F00)>>8] \n", V[(opcode&0x0F00)>>8] * 5 );
					printf( "%#x : 0xF00 + V[(opcode&0x0F00)>>8] \n\n",0xF00 + V[(opcode&0x0F00)>>8] * 5 );*/
					I = ( V[((opcode&0x0F00)>>8)] * 5 );
					PC+=2;
				break;
				
				case 0x30:
					// fr30  	xfont vr  	point I to the sprite for hexadecimal character in vr  	Superchip only: Sprite is 10 bytes high.
					I = 0x50 + ( V[((opcode&0x0F00)>>8)] * 10 );
					PC+=2;
				break;
				
				case 0x33:
        			memory[I] = (V[((opcode&0x0F00)>>8)]/100);
					memory[I+1] = ((V[((opcode&0x0F00)>>8)]/10)%10);
					memory[I+2] = ((V[((opcode&0x0F00)>>8)]%100)%10);
					/*printf( "%u : memory[I] \n", memory[I] );
					printf( "%u : memory[I+1] \n", memory[I+1] );
					printf( "%u : memory[I+2] \n", memory[I+2] );
					printf( "%u : V[(opcode&0x0F00)>>8] \n", V[((opcode&0x0F00)>>8)] );*/
					PC+=2;
	   			break;

				case 0x55:
					for( unsigned int i = 0; i <= ((opcode&0x0F00)>>8); i++ ) {
						memory[I+i] = V[i];
						//printf( "%#x : I+i \n", I+i );
					}
					//I += ((opcode&0x0F00)>>8) + 1;
					PC+=2;
				break;
				
				case 0x65:
					for( unsigned int i = 0; i <= ((opcode&0x0F00)>>8); i++ ) {
						V[i] = memory[I+i];
						//printf( "%#x : I+i \n", I+i );
					}
					//I += ((opcode&0x0F00)>>8) + 1;
					PC+=2;
				break;
				
				case 0x75:
					if( ((opcode&0x0F00)>>8) < 8 ) {
						for( unsigned int i = 0; i <= ((opcode&0x0F00)>>8); i++ ) {
							memory[0xF00+i] = V[i];
							//printf( "%#x : I+i \n", I+i );
						}
					} else {
						cout << "Error, (opcode&0x0F00)>>8) >= 8" << endl;
						printf( "%#x : (opcode&0x0F00)>>8)", ((opcode&0x0F00)>>8) ); 
					}
					PC+=2;				
				break;
				
				case 0x85:
					if( ((opcode&0x0F00)>>8) < 8 ) {
						for( unsigned int i = 0; i <= ((opcode&0x0F00)>>8); i++ ) {
							V[i] = memory[0xF00+i];
							//printf( "%#x : I+i \n", I+i );
						}
					} else {
						cout << "Error, (opcode&0x0F00)>>8) >= 8" << endl;
						printf( "%#x : (opcode&0x0F00)>>8)", ((opcode&0x0F00)>>8) ); 
					}
					PC+=2;				
				break;
				
				default:
					printf("%04x: opcode not implemented.\n", opcode);
					exit(0);
				break;
				
			}
			
		break;
		
		default:
			printf("%04x: opcode not implemented.\n", opcode);
			exit(0);
		break;
		
	}
}
