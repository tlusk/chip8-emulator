/*
 * Chip8.cpp
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.13  2007/02/06 00:40:29  timothy
 *      Added PSP Joystick Support
 *
 *      Revision 1.12  2007/02/05 19:58:12  timothy
 *      Implemented a bitset
 *      Fixed scrolling right
 *      Fixed overflows when drawing which caused crashes
 *
 *      Revision 1.11  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.10  2007/01/24 03:41:11  timothy
 *      Partially implemented schip8
 *
 *      Revision 1.9  2007/01/23 08:04:43  timothy
 *      Implemented OpenGL drawing method
 *
 *      Revision 1.8  2007/01/23 07:26:34  timothy
 *      Changed includes to remove the PSP ones when not compiling for PSP
 *
 *      Revision 1.7  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.6  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 *      Revision 1.5  2007/01/21 18:02:57  timothy
 *      Added More Games
 *
 *      Revision 1.4  2007/01/21 08:06:39  timothy
 *      Added PSP Support (doesnt work on psp, just compiles)
 *      Fixed input handling (couldnt tell when keys were held)
 *      Fixed a bug when drawing (collision not reset before drawing)
 *      Optimized drawing
 *      Limited Speed (0% CPU!)
 *
 *      Revision 1.3  2007/01/19 23:43:29  timothy
 *      Added sound
 *      Added ability to choose the rom to run
 *
 *      Revision 1.2  2007/01/18 03:25:03  timothy
 *      fixed the bug that broke drawing, added key presses
 *
 *      Revision 1.1  2007/01/18 01:53:40  timothy
 *      Initial Commit
 *
 */

#ifdef PSP
#include <pspkernel.h>
#include <pspdebug.h>
#include <psppower.h>
#include <pspdisplay.h>
#endif

#include <iostream>
#include <fstream>
#include <string>
#include "Cpu.h"
#include "Display.h"
#include "SDL/SDL_thread.h"

using namespace std;

Cpu *cpu;

#ifndef PSP
int main( int argc, char *argv[] ) {
#else
extern "C" int SDL_main(int argc, char **argv) {
	//scePowerSetClockFrequency(20, 20, 10); 
#endif

	unsigned char* memory = new unsigned char [0xFFF];
	FILE *rom;
	
	#ifndef PSP
	if( argc <= 1 ) {
		cout << "Usage: chip8 <ROM>" << endl;
		exit(0);
	}
	
	if( ( rom = fopen( argv[1], "rb" ) ) == NULL ) {
	#else
	if( ( rom = fopen( "games/WIPEOFF", "rb" ) ) == NULL ) {
	#endif
		cout << "Unable to open rom " << argv[1] << endl;
		exit(0);
	} else {
		fread( &memory[0x200], 0xFFF, 1, rom );
		fclose( rom );
	}
	
	#ifndef PSP
	bool opengl = true;
	#else
	bool opengl = false;
	#endif
	
	if( argc >= 3 ) {
		string openglFlag = argv[2];
		if( openglFlag == "--sdl" ) {
			opengl = false;
		}
	}
	
	cpu = new Cpu( memory, opengl );
	
	SDL_Event event;
	bool running = true;

	//SDL_Thread *thread = NULL;
	//thread = SDL_CreateThread( drawThread, NULL );
	
	while (running) {
	//for( int i = 0; i < 50000; i++ ) {
		/*if( i > 23000 ) {
			//SDL_Delay( 1 );
			if( i > 23320 ) {
				//SDL_Delay( 20 );
				if( i >= 23356 ) {
					//SDL_Delay( 50 );
					cout << "SEGFAULT COMING" << endl;
				}
			}
		}*/
		if (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				running = false;
			}
		}
		cpu->exec();
	}
	
	return 0;
}

int drawThread( void *data ) {
	
	while( true ) {
		#ifdef PSP
		sceDisplayWaitVblankStart();
		#else
		SDL_Delay( 1 );
		#endif
		cpu->display.draw();
	}
    
    return 0;
}
