/*
 * Input.h
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.2  2007/02/06 00:40:29  timothy
 *      Added PSP Joystick Support
 *
 *      Revision 1.1  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 */

#ifndef INPUT_H_
#define INPUT_H_

#include "SDL/SDL.h"

using namespace std;

class Input {	
    
public: // Constructors

	Input();
	
public: // Acess Methods

	unsigned char getKeyPress();
	
	bool isKeyPressed( unsigned char key );
	
	#ifdef PSP
	SDL_Joystick *joystick;
	#endif
	
}; // Input

#endif /*INPUT_H_*/
