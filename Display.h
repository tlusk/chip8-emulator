/*
 * Display.h
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.7  2007/02/05 19:58:12  timothy
 *      Implemented a bitset
 *      Fixed scrolling right
 *      Fixed overflows when drawing which caused crashes
 *
 *      Revision 1.6  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.5  2007/01/24 03:41:11  timothy
 *      Partially implemented schip8
 *
 *      Revision 1.4  2007/01/23 08:04:43  timothy
 *      Implemented OpenGL drawing method
 *
 *      Revision 1.3  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.2  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 *      Revision 1.1  2007/01/18 01:53:40  timothy
 *      Initial Commit
 *
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <bitset>

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"
#include "SDL/SDL_opengl.h"

using namespace std;

class Display {	
    
public: // Constructors

	Display( bool openglRender );
	
public: // Methods

	void draw();
	
	void drawPixel( int x, int y, bool enabled );
	
	void drawScreen( bitset<128*64> &disp );
	
	void clearScreen();
	
	void setExtendedMode( bool enabled );
	
	friend int drawThread( void *data );
	
private: // Data members
	
	SDL_Surface* screen;
	
	bool opengl;
	
	unsigned int pixelSize;
		
}; // Display

#endif /*DISPLAY_H_*/
