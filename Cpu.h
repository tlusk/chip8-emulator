/*
 * Cpu.h
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.9  2007/02/05 19:58:12  timothy
 *      Implemented a bitset
 *      Fixed scrolling right
 *      Fixed overflows when drawing which caused crashes
 *
 *      Revision 1.8  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.7  2007/01/24 03:41:11  timothy
 *      Partially implemented schip8
 *
 *      Revision 1.6  2007/01/23 08:04:43  timothy
 *      Implemented OpenGL drawing method
 *
 *      Revision 1.5  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 *      Revision 1.4  2007/01/21 08:06:39  timothy
 *      Added PSP Support (doesnt work on psp, just compiles)
 *      Fixed input handling (couldnt tell when keys were held)
 *      Fixed a bug when drawing (collision not reset before drawing)
 *      Optimized drawing
 *      Limited Speed (0% CPU!)
 *
 *      Revision 1.3  2007/01/19 23:43:29  timothy
 *      Added sound
 *      Added ability to choose the rom to run
 *
 *      Revision 1.2  2007/01/18 03:25:03  timothy
 *      fixed the bug that broke drawing, added key presses
 *
 *      Revision 1.1  2007/01/18 01:53:40  timothy
 *      Initial Commit
 *
 */

#ifndef CPU_H_
#define CPU_H_

#include <iostream>
#include <bitset>

#include "Display.h"
#include "Input.h"
#include "Audio.h"

using namespace std;

class Cpu {	
    
public: // Constructors

	Cpu( unsigned char* memory, bool opengl );
	
public: // Modification Methods

	void exec();
	
private: // Internal Methods
	
private: // Data members

	bool extendedMode;
	
	unsigned char* memory;
	
	bitset<128*64> screen;
	
	unsigned char V[16];

	unsigned int PC, I, stackPos, sTimer, dTimer;
	
	unsigned int subStack[16];
	
	Audio audio;
	
	Input input;
	
public:	
	
	Display display;
	
}; // Cpu


int drawThread( void *data );

#endif /*CPU_H_*/
