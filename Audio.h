/*
 * Audio.h
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.1  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include "SDL/SDL_mixer.h"
#include "SDL/SDL.h"

using namespace std;

class Audio {
	
public: // Constructors

	Audio();
	
public: // Modification Methods

	void beep();
	
private: // Data members
	
	Mix_Music *beepSound;
		
}; // Audio

#endif /*AUDIO_H_*/
