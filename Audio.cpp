/*
 * Audio.cpp
 *
 * Version:
 *      $Id$
 *
 * Revisions:
 *      $Log$
 *      Revision 1.4  2007/02/03 04:07:07  timothy
 *      SChip8 Fully Implemented
 *
 *      Revision 1.3  2007/01/23 07:25:27  timothy
 *      Added PSP Support
 *
 *      Revision 1.2  2007/01/22 05:57:27  timothy
 *      Changed beep.wav path
 *
 *      Revision 1.1  2007/01/22 05:55:30  timothy
 *      Changed Class Structure
 *
 */

#include <iostream>

#include "Audio.h"

Audio::Audio() {
	SDL_Init( SDL_INIT_AUDIO );
	
	int audio_rate = 22050;
  	Uint16 audio_format = AUDIO_S16; /* 16-bit stereo */
  	int audio_channels = 2;
  	int audio_buffers = 4096;
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
    	printf("Unable to open audio!\n");
  	}
  	
  	beepSound = Mix_LoadMUS("resources/beep.wav");
}

void Audio::beep() {
	Mix_PlayMusic( beepSound, 0 );
}
